package com.ijpay.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IJPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(IJPayApplication.class, args);
    }
    

}


